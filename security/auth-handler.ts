import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';
import { NotAuthorizedError } from 'restify-errors';
import { Provider } from '../providers/providers.model';
import { environment } from '../common/environment';

export const authenticate: restify.RequestHandler = (req, resp, next) => {
    const { email, password } = req.body;
    Provider.findByEmail(email, '+password').then(provider => {
        if (provider && provider.matches(password)) {
            const token = jwt.sign({sub: provider.email, iss: 'piscineiros-api'}, environment.security.apiSecret, { expiresIn: '8h' })
            const decoded = jwt.decode(token, {complete: true});
            resp.json({
                    name: provider.name,
                    email: provider.email,
                    accessToken: token,
                    providerId: provider._id,
                    profiles: provider.profiles,
                    services: provider.services,
                    exp: (decoded.payload as any).exp
                })
            return next(false)
        } else {
            return next(new NotAuthorizedError('Invalid Credentials'))
        }
    }).catch(next)

}
