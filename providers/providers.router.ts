import * as restify from 'restify'
import { ModuleRouter } from '../common/model-router';
import { Provider, IProvider } from './providers.model';
import { authenticate } from '../security/auth-handler';
import { auth } from '../security/auth';

// const multer = require('multer');
// const upload = multer({ dest: 'uploads/' });

class ProvidersRouter extends ModuleRouter<IProvider> {
    
    constructor() {
        super(Provider)
        this.on('beforeRender', document => {
            document.password = undefined;
        })
    }

    applyRoutes(application: restify.Server) {
        application.get('/providers', [auth('admin', 'provider'), this.findAll])
        application.get('/providers/:id', [auth('admin', 'provider'), this.validateId, this.findById])
        application.post('/providers', this.save)
        application.put('/providers/:id', [auth('admin', 'provider'), this.validateId, this.update])
        application.del('/providers/:id', [auth('admin', 'provider'), this.validateId, this.delete])
        application.post('/providers/authenticate', authenticate)

        // application.post('/providers/upload', upload.single('avatar'), (req, res, next) => {
        //     res.send('Olá');
        // })
    }
}

export const providersRouter = new ProvidersRouter();
