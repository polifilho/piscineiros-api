export const environment = {
    server: {
        port: process.env.SERVER_PORT || 3000,
    },    
    db: {
        url: process.env.DB_URL || 'mongodb://localhost/piscineiros',
    },
    security: {
        saltRounds: process.env.SALT_ROUNDS || 10,
        apiSecret: process.env.API_SECRET || 'piscineiros-api-secret',
        enableHTTPS: process.env.ENABLE_HTTPS || false,
        // api_email_key: process.env.MAILGUN_API_KEY || '3fbc9809bf129621664d3c3fbc27a81a-f68a26c9-dfd8905c'
        api_email_key: process.env.MAILGUN_API_KEY || 'pubkey-d53f52f6d060a362aa3cbd5850b82bab'
        
    },
    log: {
        level: process.env.LOG_LEVEL || 'debug',
        name: 'piscineiros-api-logger',
    }
}