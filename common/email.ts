import { environment } from "./environment";

const formData = require('form-data');
const Mailgun = require('mailgun.js');

export const SendEmail = (req, resp, next) => {
    const mailgun = new Mailgun(formData);
    const mg = mailgun.client({username: 'api', key: 'pubkey-d53f52f6d060a362aa3cbd5850b82bab' || 'f68a26c9-dfd8905c'});

    mg.messages.create('sandbox-123.mailgun.org', {
        from: "Excited User <mailgun@sandbox-123.mailgun.org>",
        to: ["pedropolisenso@gmail.com"],
        subject: "Hello",
        text: "Testing some Mailgun awesomeness!",
        html: "<h1>Testing some Mailgun awesomeness!</h1>"
    })
    .then((msg) => {
        return resp.send({ message: msg });
    })
    .catch((err) => {
        return resp.send({ error: err });
    });
}