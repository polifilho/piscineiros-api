import mongoose from 'mongoose';
// import { validateCPF } from '../common/validators';

export interface AddressDocument extends mongoose.Document {
    cep: {
        type: String,
        require: true,
        maxLength: 8
    },
    street: {
        type: String,
        require: true,
    },
    city: {
        type: String,
        require: true,
    },
    district: {
        type: String,
        require: true,
    },
    state: {
        type: String,
        require: true,
    },
    complement: {
        type: String,
        require: true,
    },
    number: {
        type: String,
        require: true,
    },
}

export interface ICustomer extends mongoose.Document {
    providerId: mongoose.Types.ObjectId;
    name: string;
    cpf?: string;
    cnpj?: string;
    email: string;
    mobile: string;
    address: AddressDocument;
    gender: string;
    datePaymentExpired: string;
    paymentMethod: string;
    price: number;
    services: string[]
    customerType: string;
    paymentOften: string;
    paymentStatus: string;
    serviceDays: string[];
    periodOfTime: string[];
}

const customersSchema = new mongoose.Schema({
    providerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Provider'
    },
    name: {
        type: String,
        required: true,
        minlength: 3
    },
    cpf: {
        type: String,
        // validate: {
        //     validator: validateCPF,
        //     message: 'CPF Inválido: ({VALUE})'
        // }
    },
    cnpj: {
        type: String,
        // match: /\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/
    },
    email: {
        type: String,
        required: true,
        match: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    },
    phone: {
        type: String,
        required: true,
        match: /^\(?[1-9]{2}\)? ?(?:[2-8]|9[0-9])[0-9]{3}\-?[0-9]{4}$/
    },
    address: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    gender: {
        type: String,
        enum: ['male', 'female']
    },
    datePaymentExpired: {
        type: String,
    },
    paymentMethod: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    services: {
        type: [String],
        required: true
    },
    customerType: {
        type: String,
        required: true
    },
    paymentOften: {
        type: String,
        required: true
    },
    paymentStatus: {
        type: String,
        required: true
    },
    serviceDays: {
        type: [String],
        required: true
    },
    periodOfTime: {
        type: [String],
        required: true
    },
    realDatePaymentExpired: {
        type: Number,
    },
    dateToCreateCustomer: {
        type: Number,
    },
    timeCustomerPriceSave: {
        type: String,
    },
    lastUpdate: {
        type: Number,
    }
}, 
{
    versionKey: false
})

export const Customers = mongoose.model<ICustomer>('Customers', customersSchema)
