import * as fs from 'fs';
import * as restify from 'restify'
import * as mongoose from 'mongoose';
import { environment } from '../common/environment';

import { Router } from '../common/router';
import { handleError } from './handler-error';
import { tokenParser } from '../security/token-parse';
import { logger } from '../common/logger';
import * as corsMiddleware from 'restify-cors-middleware2';
import bodyParser from "body-parser";

export class Server {
	// attributes
	application:  restify.Server

	// methods
	initDB() {
		(<any>mongoose).Promise = global.Promise
		return mongoose.connect(environment.db.url)
	}

	initRoutes(routers: Router[]): Promise<any> {
		return new Promise((resolve, reject) => {
			try {
				const options: restify.ServerOptions = {
					name: 'brum-api',
					version: '1.0.0',
					log: logger
				}

				if (environment.security.enableHTTPS) {
					options.certificate = fs.readFileSync('./security/keys/cert.pem');
					options.key = fs.readFileSync('./security/keys/key.pem');
				}

				// creating server
				this.application = restify.createServer(options)

				const corsOptions: corsMiddleware.Options = {
					preflightMaxAge: 5,
					origins: ['*'],
					allowHeaders: [
						'X-App-Version',
						'Accept',
						'Accept-Version',
						'Content-Type',
						'Api-Version',
						'Origin',
						'X-Requested-With',
						'Authorization',
					],
					exposeHeaders: [],
					credentials: true,
					allowCredentialsAllOrigins: true,
				}
				
				const cors: corsMiddleware.CorsMiddleware = corsMiddleware(corsOptions);
				
				// it will prepare the logger for each new request
				this.application.pre(cors.preflight)
				this.application.pre(restify.plugins.requestLogger({
					log: logger
				}))

				// it will called only if the route is valid
				this.application.use(cors.actual)
				this.application.use(restify.plugins.queryParser())
				this.application.use(restify.plugins.bodyParser())
				this.application.use(tokenParser)

				for (let router of routers) {
					router.applyRoutes(this.application)
				}

				this.application.listen(environment.server.port, () => {
					resolve(this.application)
				})
				
				this.application.on('restifyError', handleError)
			} catch (error) {
				reject(error)
			}
		})
	}

	bootstrap(routers: Router[] = []): Promise<Server>{
		return this.initDB().then(() => this.initRoutes(routers).then(() => this))
	}
}
